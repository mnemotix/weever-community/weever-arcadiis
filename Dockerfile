FROM node:16.13.1-alpine

RUN apk --update add --no-cache poppler poppler-utils antiword gdal gdal-tools

ARG CI

RUN mkdir -p /opt/app
WORKDIR /opt/app
ADD . /opt/app
ENV NODE_ENV production
RUN YARN_IGNORE_NODE=1 yarn set version berry
RUN YARN_IGNORE_NODE=1 yarn set version 3.1
RUN yarn install
RUN yarn build:prod
CMD yarn run start:prod
