# Pour installer toutes la stack Weever/Arcadiis sur Humanum

Se connecter sur la machine dédiée à Arcadiis sur Humanum (arcadiis.huma-num.fr)

```shell
ssh [nom d'utilisateur]@arcadiis.huma-num.fr
```

## Déployer les fichiers de conf sur la machine Humanum

Dans le répertoire `/var/www/arcadiis`, copier les fichiers :

- `humanum/docker-compose.yml` : Le fichier docker compose prêt à l'emploi permettant le démarrage de la stack de conteneurs applicatifs.
- `humanum/apache-vhosts.conf` : Le fichier de config Apache prêt à l'emploi permettant de créer un Proxy/ReverseProxy vers les différents conteneurs applicatifs.
- `humanum/.env` : Le fichier d'environnement à configurer avec les bons paramètres

Note: Dans le fichier docker-compose, des addresses IP statiques (sous réseau 172.18.0.0) sont assignées aux différents conteneurs à mettre en frontal sur le web. Ces Ips doivent correspondrent dans le fichier des vhosts Apache.


## Configurer les certificats SSL

Ne pas oublier de générer des certificats par sous nom de domaines :

```sh
certbot certonly --apache  --cert-name="arcadiis.science" -d arcadiis.science -d weever.arcadiis.science -d auth.arcadiis.science -d arcadiis.huma-num.fr -d minio.arcadiis.science -d tusd.arcadiis.science -d companion.arcadiis.science -d images.arcadiis.science
```

Note: Les certificats sont générés dans le répertoire `/etc/letsencrypt/live/arcadiis.science`

## Configurer Mysql

Mysql est disponible depuis la machine hôte et non pas comme un service Docker. Il faut donc faire une manip pour qu'un conteneur puisse y accéder. Celle-ci consiste à lier l'adresse IP d'écoute (par défaut localhost) à l'IP de gateway configurée dans le fichier docker-compose.

- Modifier le fichier my.cnf pour faire un bind-address sur l'IP de gateway précisée dans la donc docker-compose (172.18.0.1)

#### **`/etc/mysql/my.conf`**
```conf
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/

[mysqld]

bind-address = 172.18.0.1
```

## Configurer Apache
Ajouter le module mod_headers:

```sh
a2enmod headers
```

Ajouter le fichier de configuration Apache :

```sh
 ln -s /var/www/arcadiis/apache-vhosts.conf /etc/apache2/sites-enabled/apache-vhosts.conf
```

Redémarrer Apache :

```sh
service apache2 restart
```

## Démarrer la stack

Dans le dossier `/var/www/arcadiis` :

```sh
  docker-compose up -d 
```

## Initialisations pas encore automatisées :

## Minio

Se connecter à [Minio](https://minio.arcadiis.science) avec les identifiants disponibles dans le `.env`

Créer un nouveau bucker nommé `arcadiis`

## Keycloak
 
Se connecter à [Keycloak](https://auth.arcadiis.science) avec les identifiants disponibles dans le `.env`

Créer un nouveau Realm en important le fichier `/humanum/real-arcadiis.json` APREs avoir changé la ligne 7 :

```
"secret": "** NE PAS OUBLIER DE REMPLIR LE MÊME SECRET QUE DANS LE .env",
```

Mettre dans cette ligne la même valeur que celle configurée dans la variable `KEYCLOAK_CLIENT_SECRET` du `.env` après l'avoir générée sur [un UUID générateur](https://www.uuidgenerator.net/version4)
