/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {ListItemText} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {LoadingSplashScreen, PluralLinkField} from "@synaptix/ui";
import {OrganizationsAutocomplete, OrganizationFormContent} from "@mnemotix/weever-core";
import {projectReferralOrganizationsInputDefinition} from "./form/ProjectReferralOrganizations.form";
import {gqlProjectReferralOrganizations} from "./gql/ProjectReferralOrganizations.gql";

export function ProjectReferralOrganizationsEdit({projectId} = {}) {
  const {t} = useTranslation();

  const [getProjectReferralOrganizations, {data: {project} = {}, loading}] = useLazyQuery(
    gqlProjectReferralOrganizations
  );

  useEffect(() => {
    if (projectId) {
      getProjectReferralOrganizations({
        variables: {
          projectId
        }
      });
    }
  }, [projectId]);

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <PluralLinkField
      data={project}
      label={t("PROJECT.REFERRAL_ORGANIZATIONS")}
      linkInputDefinition={projectReferralOrganizationsInputDefinition}
      renderObjectContent={(organization) => {
        return <ListItemText primary={organization?.name} />;
      }}
      renderObjectAutocomplete={({...props}) => {
        return <OrganizationsAutocomplete {...props} />;
      }}
      renderObjectForm={() => <OrganizationFormContent />}
      addButtonLabel={t("PROJECT.REFERRAL_ORGANIZATIONS_ADD")}
    />
  );
}
