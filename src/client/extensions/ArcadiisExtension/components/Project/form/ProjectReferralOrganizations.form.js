import {LinkInputDefinition} from "@synaptix/ui";
import {organizationFormDefinition} from "@mnemotix/weever-core";

export const projectReferralOrganizationsInputDefinition = new LinkInputDefinition({
  name: "referralOrganizations",
  isPlural: true,
  inputName: "referralOrganizationInputs",
  targetObjectFormDefinition: organizationFormDefinition,
  forceUpdateTarget: false
});
