/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {useLazyQuery} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {Typography, Box} from "@mui/material";
import {formatRoute} from "react-router-named-routes";
import {createLink, ExcerptPopover, LoadingSplashScreen} from "@synaptix/ui";
import {globalSx, OrganizationExcerpt, ROUTES} from "@mnemotix/weever-core";
import {gqlProjectReferralOrganizations} from "./gql/ProjectReferralOrganizations.gql";

export function ProjectReferralOrganizations({projectId} = {}) {
  const {t} = useTranslation();
  const [loadProject, {data: {project} = {}, loading} = {}] = useLazyQuery(gqlProjectReferralOrganizations);

  useEffect(() => {
    if (projectId) {
      loadProject({
        variables: {
          projectId
        }
      });
    }
  }, [projectId]);

  return (
    <Choose>
      <When condition={loading}>
        <LoadingSplashScreen />
      </When>
      <When condition={project?.referralOrganizations?.edges.length > 0}>
        <Typography variant="subtitle1">{t("PROJECT.REFERRAL_ORGANIZATIONS")}</Typography>
        {project.referralOrganizations.edges.map(({node: organization}) => (
          <Box component="span" sx={globalSx.commaAfter} key={organization.id}>
            <ExcerptPopover OnHoverDisplayComponent={<OrganizationExcerpt id={organization.id} />}>
              {createLink({
                text: organization.name,
                to: formatRoute(ROUTES.ORGANIZATION, {id: organization.id})
              })}
            </ExcerptPopover>
          </Box>
        ))}
      </When>
    </Choose>
  );
}
