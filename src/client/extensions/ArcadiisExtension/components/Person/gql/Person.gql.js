import {gql} from "@apollo/client";

export const gqlPersonFragment = gql`
  fragment PersonArcadiisFragment on Person {
    id
    avatar
    firstName
    lastName
    fullName
    bio
    bioTranslated
    shortBio
    shortBioTranslated
    birthday
    halId
    orcidId
  }
`;
