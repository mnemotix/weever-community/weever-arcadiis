import {personFormDefinition, agentEmailLinkInputDefinition} from "@mnemotix/weever-core";
import {object, array, string} from "yup";
import {gqlPersonFragment} from "../gql/Person.gql";

personFormDefinition.mutationConfig.scalarInputNames.push("halId", "orcidId");
personFormDefinition.mutationConfig.gqlFragment = gqlPersonFragment;
personFormDefinition.validationSchema = ({t}) => {
  return object().shape({
    lastName: string().required(t("PERSON.FIELD_ERRORS.LAST_NAME_REQUIRED")),
    emails: object()
      .default({edges: []})
      .shape({
        edges: array().min(1, t("PERSON.FIELD_ERRORS.EMAIL_REQUIRED"))
      })
  });
};
personFormDefinition.mutationConfig.linkInputDefinitions = [agentEmailLinkInputDefinition];

export default personFormDefinition;
