import {useEffect} from "react";
import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {RichTextAreaField, TextField, TranslatableField, SelectField, useFormController} from "@synaptix/ui";
import {ResourceReferralPersonsEdit} from "./ReferralPersons/ResourceReferralPersonsEdit";
import {resourceFormDefinition} from "./form/Resource.form";

export default function ResourceFormContentBatch({resource} = {}) {
  const {t} = useTranslation();
  const formController = useFormController();

  useEffect(() => {
    formController.alterFormDefinition(resourceFormDefinition);
  }, []);

  return (
    <>
      <Grid item xs={12}>
        <TranslatableField
          FieldComponent={RichTextAreaField}
          name="description"
          label={t("RESOURCE.RESUME")}
          helperText={t("RESOURCE.RESUME_HELPER_TEXT")}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          name="temporalExtent"
          label={t("RESOURCE.TEMPORAL_EXTENT")}
          helperText={t("RESOURCE.TEMPORAL_EXTENT_HELPER_TEXT")}
        />
      </Grid>

      <Grid item xs={12}>
        <ResourceReferralPersonsEdit resourceId={resource?.id} />
      </Grid>

      <Grid item xs={12}>
        <SelectField
          fullWidth
          FieldComponent={RichTextAreaField}
          name="rights"
          label={t("RESOURCE.RIGHTS")}
          options={[
            {
              label: "CC BY-SA",
              value: "CC BY-SA"
            },
            {
              label: "Licence ouverte Etalab",
              value: "Licence ouverte Etalab"
            },
            {
              label: "ODbL",
              value: "ODbL"
            }
          ]}
        />
      </Grid>

      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="notes" label={t("RESOURCE.NOTES")} />
      </Grid>
    </>
  );
}
