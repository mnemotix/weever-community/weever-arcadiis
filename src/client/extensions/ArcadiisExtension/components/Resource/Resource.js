/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useParams} from "react-router-dom";
import {Button as MuiButton, Grid, Typography, Box} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";

import {formatRoute} from "react-router-named-routes";
import numeraljs from "numeraljs";
import {
  Button,
  createLink,
  EntityAccessPolicy,
  LoadingSplashScreen,
  RichTextViewer,
  Taggings,
  ResourceViewer
} from "@synaptix/ui";
import {ROUTES, dayjs, ResourceBreadcrumb, EntityActivity} from "@mnemotix/weever-core";

import {gqlResource} from "./gql/Resource.gql";
import {ResourceReferralPersons} from "./ReferralPersons/ResourceReferralPersons";

const classes = {
  resourceViewerContainer: {
    textAlign: "center",
    marginBottom: 4
  },
  empty: (theme) => ({
    color: theme.palette.text.emptyHint
  }),
  filename: {
    wordBreak: "break-all"
  }
};

export default function Resource(props) {
  const {t} = useTranslation();

  let {id} = useParams();
  id = decodeURIComponent(id);

  const {data: {resource} = {}, loading, fetchMore} = useQuery(gqlResource, {
    variables: {
      id
    }
  });

  const projectContributionId = props?.location?.state?.projectContributionId;

  return loading || !resource ? (
    <LoadingSplashScreen />
  ) : (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <ResourceBreadcrumb resource={resource} forceProjectContributionId={projectContributionId} />
      </Grid>
      <Grid item xs={12} container columnSpacing={4}>
        <Grid item xs={12} md={6} container>
          <Grid item xs={12} sx={classes.resourceViewerContainer}>
            <ResourceViewer resource={resource} />
          </Grid>

          <Grid item xs={12} container spacing={2} alignItems={"center"}>
            <Grid item xs />
            <Grid item>
              <div>
                {t("RESOURCE.CREATED_AT_BY", {
                  date: dayjs(resource?.createdAt).format("LL")
                })}

                {createLink({
                  to: formatRoute(ROUTES.PERSON, {
                    id: resource?.creatorPerson?.id
                  }),
                  text: resource?.createdBy
                })}
              </div>
            </Grid>
            <Grid item>
              <Button
                locked={!resource?.permissions?.update}
                variant="contained"
                color="primary"
                sxClass={{
                  backgroundColor: "#184A58 !important",
                  color: "#ffffff"
                }}
                to={{
                  pathname: formatRoute(ROUTES.RESOURCE_EDIT, {id}),
                  state: {
                    projectContributionId
                  }
                }}>
                {t("ACTIONS.UPDATE")}
              </Button>
            </Grid>
            <Grid item>
              <MuiButton
                variant="outlined"
                component="a"
                href={resource?.publicUrl}
                target="_blank"
                sx={{
                  color: "#184A58",
                  borderColor: "#184A58"
                }}>
                {t("ACTIONS.DOWNLOAD")}
              </MuiButton>
            </Grid>
            <Grid item xs />
          </Grid>
        </Grid>

        <Grid item container md={6} spacing={3} alignContent="flex-start">
          <Grid item xs={12} md={6}>
            <Typography variant="subtitle1" gutterBottom>
              {t("RESOURCE.FILE_NAME")}
            </Typography>
            <Box component="div" textOverflow="ellipsis" overflow="hidden" sx={classes.filename}>
              {resource?.filename}
            </Box>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="subtitle1" gutterBottom>
              {t("RESOURCE.MIME")}
            </Typography>
            <Box component="div" textOverflow="ellipsis" overflow="hidden">
              {resource?.mime}
            </Box>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="subtitle1" gutterBottom>
              {t("RESOURCE.SIZE")}
            </Typography>
            <div>{numeraljs(resource?.size).format("0b")}</div>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="subtitle1">{t("ENTITY.ACCESS_POLICY")}</Typography>
            <EntityAccessPolicy entityId={id} />
          </Grid>

          <Grid item xs={12}>
            <Typography variant="subtitle1" gutterBottom>
              {t("RESOURCE.URL")}
            </Typography>
            <a href={resource?.publicUrl} target="_blank">
              {resource?.publicUrl}
            </a>
          </Grid>

          <Grid item xs={12}>
            <Typography variant="subtitle1" gutterBottom>
              {t("RESOURCE.TITLE")}
            </Typography>

            <Box sx={!resource?.title && classes.empty}>{resource?.title || t("RESOURCE.EMPTY_FIELD")}</Box>
          </Grid>

          <If condition={resource?.altTitle}>
            <Grid item xs={12}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.ALT_TITLE")}
              </Typography>

              <Box sx={!resource?.altTitle && classes.empty}>{resource?.altTitle || t("RESOURCE.EMPTY_FIELD")}</Box>
            </Grid>
          </If>

          <If condition={resource?.description}>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("RESOURCE.RESUME")}</Typography>

              <Box sx={!resource?.description && classes.empty}>
                <RichTextViewer content={resource?.description || t("RESOURCE.EMPTY_FIELD")} />
              </Box>
            </Grid>
          </If>

          <If condition={resource?.tableOfContents}>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("RESOURCE.TABLE_OF_CONTENTS")}</Typography>

              <Box sx={!resource?.tableOfContents && classes.empty}>
                <RichTextViewer content={resource?.tableOfContents || t("RESOURCE.EMPTY_FIELD")} />
              </Box>
            </Grid>
          </If>

          <Grid item xs={12}>
            <Typography variant="subtitle1">{t("ENTITY.TAGGINGS")}</Typography>
            <Taggings entityId={id} />
          </Grid>

          <If condition={resource?.temporalExtent}>
            <Grid item xs={12}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.TEMPORAL_EXTENT")}
              </Typography>

              <Box sx={!resource?.temporalExtent && classes.empty}>
                {resource?.temporalExtent || t("RESOURCE.EMPTY_FIELD")}
              </Box>
            </Grid>
          </If>

          <Grid item xs={12}>
            <Typography variant="subtitle1" gutterBottom>
              {t("RESOURCE.REFERRAL_PERSONS")}
            </Typography>

            <Box sx={!resource?.contact && classes.empty}>
              <ResourceReferralPersons resourceId={resource?.id} />
            </Box>
          </Grid>

          <If condition={resource?.rights}>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("RESOURCE.RIGHTS")}</Typography>

              <Box sx={!resource?.rights && classes.empty}>
                <RichTextViewer content={resource?.rights || t("RESOURCE.EMPTY_FIELD")} />
              </Box>
            </Grid>
          </If>

          <If condition={resource?.notes}>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("RESOURCE.NOTES")}</Typography>

              <Box sx={!resource?.notes && classes.empty}>
                <RichTextViewer content={resource?.notes || t("RESOURCE.EMPTY_FIELD")} />
              </Box>
            </Grid>
          </If>
        </Grid>
      </Grid>
      <If condition={resource?.permissions?.grant}>
        <Grid item xs={12}>
          <EntityActivity entityId={id} />
        </Grid>
      </If>
    </Grid>
  );
}
