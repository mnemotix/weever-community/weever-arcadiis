import {LinkInputDefinition} from "@mnemotix/synaptix-client-toolkit";
import personFormDefinition from "../../../Person/form/Person.form";
/**
 * @type {LinkInputDefinition}
 */
export const resourceReferralPersonInputDefinition = new LinkInputDefinition({
  name: "referralPersons",
  isPlural: true,
  inputName: "referralPersonInputs",
  targetObjectFormDefinition: personFormDefinition,
  forceUpdateTarget: true
});
