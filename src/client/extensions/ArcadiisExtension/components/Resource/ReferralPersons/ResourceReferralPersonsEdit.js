/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {LoadingSplashScreen, PluralLinkField} from "@synaptix/ui"
import {
  PersonFormContent,
  PersonsAutocomplete,
  AgentEmailsEdit,
  PersonAvatar
} from "@mnemotix/weever-core";
import {resourceReferralPersonInputDefinition} from "./form/ResourceReferralPersons.form";
import {ListItemText, Grid, ListItemAvatar} from "@mui/material";

import {gqlResourceReferralPersons} from "./gql/ResourceReferralPersons.gql";

export function ResourceReferralPersonsEdit({resourceId, required} = {}) {
  const {t} = useTranslation();
  const [getReferralPersons, {data: {file} = {}, loading} = {}] = useLazyQuery(gqlResourceReferralPersons);

  useEffect(() => {
    if (resourceId) {
      getReferralPersons({
        variables: {
          resourceId
        }
      });
    }
  }, [resourceId]);

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <PluralLinkField
      data={file}
      linkInputDefinition={resourceReferralPersonInputDefinition}
      renderObjectContent={(person) => (
        <>
          <ListItemAvatar>
            <PersonAvatar person={person} />
          </ListItemAvatar>
          <ListItemText primary={`${person.firstName || ""} ${person.lastName || ""}`} />
        </>
      )}
      renderObjectForm={({object}) => (
        <PersonFormContent>
          <Grid item xs={12}>
            <AgentEmailsEdit agentId={object?.id} required={required} />
          </Grid>
        </PersonFormContent>
      )}
      renderObjectAutocomplete={({...props}) => {
        return <PersonsAutocomplete {...props} />;
      }}
      required={required}
      label={t("RESOURCE.REFERRAL_PERSONS")}
      addButtonLabel={t("RESOURCE.ADD_CONTACT")}
      dense
    />
  );
}
