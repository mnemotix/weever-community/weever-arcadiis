/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {formatRoute} from "react-router-named-routes";
import {useLazyQuery} from "@apollo/client";
import {ExcerptPopover, createLink, LoadingSplashScreen, } from "@synaptix/ui"
import {PersonExcerpt, ROUTES, PersonAvatar} from "@mnemotix/weever-core";
import {List, ListItem, ListItemText, ListItemAvatar} from "@mui/material";
import {gqlResourceReferralPersons} from "./gql/ResourceReferralPersons.gql";
import {useEffect} from "react";

/**
 * Display the sub projects of a given project
 * @param {string} resourceId
 */
export function ResourceReferralPersons({resourceId} = {}) {
  const {t} = useTranslation();
  const [getResourceReferralPersons, {data: {file} = {}, loading}] = useLazyQuery(gqlResourceReferralPersons);

  useEffect(() => {
    if (resourceId) {
      getResourceReferralPersons({
        variables: {
          resourceId: resourceId
        }
      });
    }
  }, [resourceId]);

  return (
    <Choose>
      <When condition={loading}>
        <LoadingSplashScreen />
      </When>
      <Otherwise>
        <List dense disablePadding>
          {(file?.referralPersons?.edges || []).map(({node: person}, index) => (
            <ListItem key={index}>
              <ListItemAvatar>
                <PersonAvatar person={person} />
              </ListItemAvatar>
              <ListItemText
                primary={
                  <ExcerptPopover OnHoverDisplayComponent={<PersonExcerpt id={person.id} />}>
                    {createLink({
                      to: formatRoute(ROUTES.PERSON, {
                        id: person.id
                      }),
                      text: person.fullName
                    })}
                  </ExcerptPopover>
                }
                secondary={""}
              />
            </ListItem>
          ))}
          <If condition={(file?.referralPersons?.edges || []).length === 0}>
            <ListItem disabled>
              <ListItemText>{t("RESOURCE.NO_CONTACT")}</ListItemText>
            </ListItem>
          </If>
        </List>
      </Otherwise>
    </Choose>
  );
}
