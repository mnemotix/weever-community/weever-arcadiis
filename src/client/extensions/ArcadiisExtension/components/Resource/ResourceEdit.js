/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {useHistory, useParams} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {useMutation, useLazyQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {LoadingSplashScreen} from "@synaptix/ui";
import {Grid} from "@mui/material";
import {ResourceBreadcrumb, ROUTES, RemoveEntityPanel} from "@mnemotix/weever-core";

import {gqlUpdateResource} from "./gql/UpdateResource.gql";
import {gqlResource} from "./gql/Resource.gql";
import {ResourceDynamicForm} from "./ResourceDynamicForm";

export default function ResourceEdit(props) {
  let {id} = useParams();
  id = decodeURIComponent(id);
  let history = useHistory();
  const {t} = useTranslation();

  const [getResource, {data: {resource} = {}, loading} = {}] = useLazyQuery(gqlResource);
  useEffect(() => {
    if (id) {
      getResource({variables: {id}});
    }
  }, [id]);

  const projectContributionId = props?.location?.state?.projectContributionId;

  const [mutateResource, {loading: savingMutation}] = useMutation(gqlUpdateResource, {
    onCompleted() {
      if (history) {
        history.push(formatRoute(ROUTES.RESOURCE, {id}));
      }
    }
  });

  return loading || !resource ? (
    <LoadingSplashScreen />
  ) : (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <ResourceBreadcrumb resource={resource} forceProjectContributionId={projectContributionId} />
      </Grid>

      <Grid item xs={12}>
        <ResourceDynamicForm
          resource={resource}
          mutateFunction={mutateResource}
          saving={savingMutation}
          renderExtraActions={renderExtraActions}
        />
      </Grid>
    </Grid>
  );

  function renderExtraActions() {
    if (resource?.permissions.delete) {
      return (
        <RemoveEntityPanel
          entity={resource}
          panelTitle={t("RESOURCE.ACTIONS.REMOVE.TITLE")}
          panelWarnMessage={t("RESOURCE.ACTIONS.REMOVE.WARN_MESSAGE")}
          confirmMessage={t("RESOURCE.ACTIONS.REMOVE.CONFIRM_MESSAGE", {resource: resource.filename})}
          removeButtonLabel={t("RESOURCE.ACTIONS.REMOVE.TITLE")}
          onSuccess={() => history.go(-2)}
          refetchQueriesAfterSuccess={["Resources"]}
        />
      );
    }
  }
}
