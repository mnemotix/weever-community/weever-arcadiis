import {Grid, Box} from "@mui/material";

import {DynamicForm, ResourceViewer} from "@synaptix/ui";
import {resourceFormDefinition} from "./form/Resource.form";
import {ResourceFormContent} from "./ResourceFormContent";

export function ResourceDynamicForm({resource, mutateFunction, saving, renderExtraActions} = {}) {
  return (
    <DynamicForm
      object={resource}
      formDefinition={resourceFormDefinition}
      mutateFunction={mutateFunction}
      saving={saving}
      renderExtraActions={renderExtraActions}>
      <Grid container columnSpacing={6} alignItems={"flex-start"}>
        <Grid item xs={12} md={4} lg={5}>
          <Box sx={classes.resourceViewerContainer}>
            <ResourceViewer resource={resource} />
          </Box>
        </Grid>

        <Grid item xs={12} md={8} lg={7} container spacing={4}>
          <ResourceFormContent resource={resource} />
        </Grid>
      </Grid>
    </DynamicForm>
  );
}

const classes = {
  resourceViewerContainer: {
    textAlign: "center"
  }
};
