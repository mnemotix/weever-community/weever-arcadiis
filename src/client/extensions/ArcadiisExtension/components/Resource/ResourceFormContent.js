import {useEffect} from "react";
import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {
  AccessPolicyField,
  RichTextAreaField,
  TaggingsField,
  TextField,
  TranslatableField,
  SelectField,
  useFormController
} from "@synaptix/ui";
import {ResourceReferralPersonsEdit} from "./ReferralPersons/ResourceReferralPersonsEdit";
import {resourceFormDefinition} from "./form/Resource.form";

export function ResourceFormContent({resource} = {}) {
  const {t} = useTranslation();
  const formController = useFormController();

  useEffect(() => {
    formController.alterMutationConfig(resourceFormDefinition.mutationConfig);
  }, []);

  return (
    <>
      <Grid item xs={12}>
        <AccessPolicyField entityId={resource?.id} />
      </Grid>

      <Grid item xs={12}>
        <TextField required={true} name="title" label={t("RESOURCE.TITLE")} />
      </Grid>

      <Grid item xs={12}>
        <TextField name="altTitle" label={t("RESOURCE.ALT_TITLE")} />
      </Grid>

      <Grid item xs={12}>
        <TranslatableField
          FieldComponent={RichTextAreaField}
          name="description"
          label={t("RESOURCE.RESUME")}
          helperText={t("RESOURCE.RESUME_HELPER_TEXT")}
        />
      </Grid>

      <Grid item xs={12}>
        <TranslatableField
          FieldComponent={RichTextAreaField}
          name="tableOfContents"
          label={t("RESOURCE.TABLE_OF_CONTENTS")}
        />
      </Grid>

      <Grid item xs={12}>
        <TaggingsField entityId={resource?.id} />
      </Grid>

      <Grid item xs={12}>
        <TextField
          name="temporalExtent"
          label={t("RESOURCE.TEMPORAL_EXTENT")}
          helperText={t("RESOURCE.TEMPORAL_EXTENT_HELPER_TEXT")}
        />
      </Grid>

      <Grid item xs={12}>
        <ResourceReferralPersonsEdit resourceId={resource?.id} required />
      </Grid>

      <Grid item xs={12}>
        <SelectField
          fullWidth
          required={true}
          name="rights"
          label={t("RESOURCE.RIGHTS")}
          options={[
            {
              label: "CC BY-SA",
              value: "CC BY-SA"
            },
            {
              label: "Licence ouverte Etalab",
              value: "Licence ouverte Etalab"
            },
            {
              label: "ODbL",
              value: "ODbL"
            }
          ]}
        />
      </Grid>

      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="notes" label={t("RESOURCE.NOTES")} />
      </Grid>
    </>
  );
}
