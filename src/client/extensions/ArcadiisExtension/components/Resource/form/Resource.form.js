import {object, string, array} from "yup";
import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {gqlResourceFragment} from "../gql/Resource.gql";

export function getResourceValidationSchema({t}) {
  return object().shape({
    title: string().required(t("RESOURCE.FIELD_ERRORS.TITLE_REQUIRED")),
    rights: string().required(t("RESOURCE.FIELD_ERRORS.RIGHTS_REQUIRED")),
    referralPersons: object()
      .default({edges: []})
      .shape({
        edges: array().min(1, t("RESOURCE.FIELD_ERRORS.CONTACT_REQUIRED"))
      })
  });
}

export const resourceFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: [
      "title",
      "altTitle",
      "description",
      "tableOfContents",
      "duration",
      "temporalExtent",
      "spatialResolution",
      "context",
      "rights",
      "notes"
    ],
    gqlFragment: gqlResourceFragment,
    gqlFragmentName: "ResourceFragment"
  }),
  validationSchema: getResourceValidationSchema
});
