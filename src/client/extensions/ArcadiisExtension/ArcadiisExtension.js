/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ROUTES, OrganizationExcerptInline, PersonExcerptInline} from "@mnemotix/weever-core";
import {Extension} from "@synaptix/ui";
import {Route} from "react-router-dom";
import loadable from "@loadable/component";

/**
 * Routes are code splitted
 */
const Resource = loadable(() => import("./components/Resource/Resource"));
const ResourceEdit = loadable(() => import("./components/Resource/ResourceEdit"));
const Project = loadable(() => import("./components/Project/Project"));
const ProjectEdit = loadable(() => import("./components/Project/ProjectEdit"));
const ResourceFormContentBatch = loadable(() => import("./components/Resource/ResourceFormContentBatch"));

export const arcadiisExtension = new Extension({
  name: "ResourceExtension",
  generateTopRoutes: ({isContributor} = {}) => (
    <>
      <Route exact path={ROUTES.RESOURCE} component={Resource} />

      <If condition={isContributor}>
        <Route exact path={ROUTES.RESOURCE_EDIT} component={ResourceEdit} />
      </If>
    </>
  ),
  fragments: {
    "Project.Heading.AfterTags": Project,
    ProjectEdit: ProjectEdit,
    "Resource.FormContentBatch.BeforeCommonFields": {
      override: true,
      Component: ResourceFormContentBatch
    }
  },
  settings: {
    "PROJECT.ACTIVITY.COMPONENT_MAPPING": {
      REFERRAL_ORGANIZATIONS: OrganizationExcerptInline
    },
    "RESOURCE.ACTIVITY.COMPONENT_MAPPING": {
      REFERRAL_PERSONS: PersonExcerptInline
    }
  }
});
