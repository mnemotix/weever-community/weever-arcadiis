/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Helmet} from "react-helmet";
import {Box} from "@mui/material";
import favicon from "../assets/favicon.png";
import logo from "../assets/logo.png";

const classes = {
  logo: (theme) => ({
    width: theme.spacing(22),
    height: "auto",
    verticalAlign: "middle",
    marginRight: theme.spacing(2)
  }),
  title: {
    verticalAlign: "middle"
  }
};

export function KonceptTitleComponent() {
  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Koncept - ArcaDIIS</title>
        <link rel="icon" type="image/png" href={favicon} sizes="32x32" />
      </Helmet>

      <Box component="img" sx={classes.logo} src={logo} alt={"ArcaDIIS"} />
      <Box component="span" sx={classes.title}>KONCEPT</Box>
    </div>
  );
}
