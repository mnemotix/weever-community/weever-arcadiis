/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const linkColor = "#b2adb5";
const white = "#ffffff";
const blue = "#184A58";
const orange = "#F78836";

export const theme = {
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: blue
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      main: blue,
      contrastText: white
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
    text: {
      emptyHint: "#474747"
    },
    background: {
      dark: '#F4F6F8',
      default: "#fafafa",
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 700,
      lg: 1080,
      xl: 1920
    }
  },
  typography: {
    subtitle1: {
      fontSize: "1rem"
    }
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        a: {
          color: linkColor
        }
      }
    },
    MuiAppBar: {
      styleOverrides: {
        colorPrimary: {
          backgroundColor: blue,
          color: white
        },
        colorSecondary: {
          backgroundColor: orange,
          color: orange
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          color: linkColor,
          textDecoration: "none"
        }
      }
    },
    MuiIconButton: {
      styleOverrides: {
        colorPrimary: {
          color: orange
        },
        colorInherit: {
          color: white
        }
      }
    },
    MuiSvgIcon: {
      styleOverrides: {
        colorPrimary: {
          color: white
        },
        colorSecondary: {
          color: orange
        }
      }
    },
    MuiTypography: {
      styleOverrides: {
        colorSecondary: {
          color: orange
        }
      }
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          fontSize: "0.875rem",
          lineHeight: 1.43
        }
      }
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: "inherit",
          fontSize: "1rem"
        }
      }
    },

    MuiButtonBase: {
      styleOverrides: {
        root: {
          color: blue
        },
        colorInherit: {
          color: white
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        textSecondary: {
          color: orange
        },
        containedSecondary: {
          color: white,
          backgroundColor: orange
        },
      }
    },
    MuiOutlinedInput: {
      styleOverrides: {
        notchedOutline: {
          "& legend": {
            fontSize: "0.70rem",
          }
        },
      }
    },

  }
};

/*  
  import {experimental_sx as sx} from '@mui/material/styles';
  MuiTableCell: {
    styleOverrides: {
      root: sx({
        px: 2,
        py: 4,
      })
    }
  },
*/