export const fr = {
  ENTITY: {
    TAGGINGS: "Mots-clés",
  },
  RESOURCE: {
    SUBHEADER: {
      METADATA: "Métadonnées du fichier",
      PRESENTATION: "Présentation de la ressource",
      COVER: "Couverture et catégorisation de la ressource",
      CONTEXT: "Cadre institutionnel et origine de la ressource",
    },
    TITLE: "Titre",
    RESUME: "Résumé",
    RESUME_HELPER_TEXT: "Présentation de la ressource",
    ALT_TITLE: "Titre alternatif",
    TABLE_OF_CONTENTS: "Table des matières",
    SUBMISSION_DATE: "Date de soumission",
    REVISION_DATE: "Date de révision",
    FORMAT: "Format",
    RESOURCE_TYPE: "Type de ressource",
    LOCATION: "Couverture spatiale",
    SPATIAL_RESOLUTON: "Résolution spatiale",
    TEMPORAL_EXTENT: "Date de création de la ressource",
    TEMPORAL_EXTENT_HELPER_TEXT:
      "NB : différente de la période temporelle concernée, ou de la date de versement dans ArcaDIIS !",
    RIGHTS: "Licence d'utilisation",
    DURATION: "Dimensions / Durée",
    EMPTY_FIELD: "Non renseigné",
    TAGGINGS: "Mot clés",
    CREATOR_ORG: "Créateur (Institution)",
    HAL_PROJECT_ID: "Id HAL du projet",
    CONTEXT: "Contexte",
    NOTES: "Notes",
    REFERRAL_PERSONS: "Contact(s) de référence",
    NO_CONTACT: "Aucun contact",
    ADD_CONTACT: "Ajouter un contact",
    FIELD_ERRORS: {
      TITLE_REQUIRED: "Le titre doit être renseigné",
      DESCRIPTION_REQUIRED: "La description doit être renseignée",
      RIGHTS_REQUIRED: "La license d'utilisateur doit être renseignée",
      CONTACT_REQUIRED: "Au moins un contact de référence doit être renseigné",
    },
    ACTIVITY: {
      REFERRAL_PERSONS: {
        ADDED: "Le contact de référence <linkedComponent/> a été ajouté.",
        ADDED_plural:
          "Les contacts de référence <linkedComponent/> ont été ajoutés.",
        REMOVED: "Le contact de référence <linkedComponent/> a été retiré.",
        REMOVED_plural:
          "Les contacts de référence <linkedComponent/> ont été retirés.",
      },
    },
  },
  PERSON: {
    ORCID_ID: "Identifiant ORCID",
    HAL_ID: "Identifiant HAL",
    FIELD_ERRORS: {
      EMAIL_REQUIRED: "Au moins une adresse mail doit être renseignée",
    },
  },
  PROJECT: {
    REFERRAL_ORGANIZATIONS: "Institution(s) de référence",
    REFERRAL_ORGANIZATIONS_ADD: "Ajouter une institution",
    ACTIVITY: {
      REFERRAL_ORGANIZATIONS: {
        ADDED: "L'institution de référence <linkedComponent/> a été ajoutée.",
        ADDED_plural:
          "Les institutions de référence <linkedComponent/> ont été ajoutées.",
        REMOVED: "L'institution de référence <linkedComponent/> a été retirée.",
        REMOVED_plural:
          "Les institutions de référence <linkedComponent/> ont été retirées.",
      },
    },
  },
};
