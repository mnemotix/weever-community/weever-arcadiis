export const en = {
  RESOURCE: {
    FILE_METADATA: "Métadonnées du fichier",
    TITLE: "Titre",
    DESCRIPTION: "Résumé",
    ALT_TITLE: "Titre alternatif",
    TABLE_OF_CONTENTS: "Table des matières",
    SUBMISSION_DATE: "Date de soumission",
    REVISION_DATE: "Date de révision",
    FORMAT: "Format",
    RESOURCE_TYPE: "Type de ressource",
    LOCATION: "Couverture spatiale",
    TEMPORAL_EXTENT: "Couverture temporelle",
    TEMPORAL_EXTENT_HELPER_TEXT:
      "NB : différente de la période temporelle concernée, ou de la date de versement dans ArcaDIIS !",
    RIGHTS: "Droits et licence",
    DURATION: "Dimensions",
    EMPTY_FIELD: "Non renseigné",
    TAGGINGS: "Mot clés",
    CREATOR_ORG: "Créateur (Institution)",
    REFERRAL_PERSONS: "Contact de référence",
    CONTEXT: "Contexte",
    NOTES: "Notes",
    NO_CONTACT: "Aucun contact",
    ADD_CONTACT: "Ajouter un contact",
    FIELD_ERRORS: {
      TITLE_REQUIRED: "Title is required",
      DESCRIPTION_REQUIRED: "Description is required",
      RIGHTS_REQUIRED: "Rights are required",
      CONTACT_REQUIRED: "At least one contact is required",
    },
  },
  PERSON: {
    ORCID_ID: "ORCID identifiant",
    HAL_ID: "HAL identifiant",
    FIELD_ERRORS: {
      EMAIL_REQUIRED: "At least one mail address must be filled",
    },
  },
  PROJECT: {
    REFERRAL_ORGANIZATIONS: "Referral institution(s)",
    REFERRAL_ORGANIZATIONS_ADD: "Add an institution",
  },
};
