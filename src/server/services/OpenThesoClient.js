/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import got from "got";
import { Model } from "@mnemotix/synaptix.js";
import { createCache } from "./cache/Cache";
import dayjs from "dayjs";

// TTL = 1 hour
const cache = createCache({ ttl: 3600 });

class OpenThesoClient {
  /**
   * Make an API Request
   * @param {string} service - OpenWeather API service name (IE: onecall)
   * @param {object} [params]  - OpenWeather API service params
   * @param {string} [openWeatherKey] - OpenWeather key
   * @return {object}
   */
  async get({ service, params = {} } = {}) {
    params.cacheBuster = dayjs().format("YYYYMMDDHH");

    const serializedParams = Object.entries(params)
      .reduce((acc, [key, value]) => {
        if (Array.isArray(value)) {
          for (let valueFragment of value) {
            acc.push(`${key}=${valueFragment}`);
          }
        } else {
          acc.push(`${key}=${value}`);
        }
        return acc;
      }, [])
      .join("&");

    const uri = `https://pactols.frantiq.fr/opentheso/api/${service}?${serializedParams}`;

    if (cache.has(uri)) {
      return cache.get(uri);
    }

    let response = await got(uri, { retry: 3 }).json();

    if (response) {
      cache.set(uri, response);
      return response;
    }
  }

  /**
   * Make an API Request
   * @param {string} id
   * @return {Promise<any>}
   */
  async getConcept({ id } = {}) {
    const service = `${id.replace("https://ark.frantiq.fr/ark:/", "")}.json`;

    let response = await this.get({
      service,
      params: {},
    });

    if (response) {
      const concept = new Model({
        id,
        uri: id,
        props: Object.entries(Object.values(response)?.[0] || {}).reduce(
          (acc, [rdfProp, jsonValue]) => {
            if (rdfProp.includes("http://www.w3.org/2004/02/skos/core")) {
              acc[
                rdfProp.replace("http://www.w3.org/2004/02/skos/core#", "")
              ] = jsonValue;
            }

            return acc;
          },
          {}
        ),
        type: "Concept",
      });

      concept.schemePrefLabel = "Opentheso";
      return concept;
    }
  }

  /**
   * Make an API Request
   * @param {string} id
   * @return {Promise<any>}
   */
  async searchConcepts({ qs, lang } = {}) {
    let response = await this.get({
      service: `autocomplete/${qs || "a"}`,
      params: {
        lang,
        theso: "TH_1",
      },
    });

    let concepts = [];

    if (response) {
      concepts = response.map(
        ({ uri, label }) =>
          new Model({
            id: uri,
            uri,
            props: {
              prefLabel: label,
              schemePrefLabel: "Opentheso",
            },
            type: "Concept",
          })
      );
    }

    return concepts;
  }
}

export const openThesoClient = new OpenThesoClient();
