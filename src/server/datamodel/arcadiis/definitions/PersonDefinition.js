/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  ModelDefinitionAbstract,
  MnxOntologies,
  LiteralDefinition,
  GraphQLTypeDefinition,
} from "@mnemotix/synaptix.js";

/**
 * Concepts are extractred from PACTOLS thesaurus.
 *
 * @see https://www.frantiq.fr/pactols/le-thesaurus/
 */
export default class PersonDefinition extends ModelDefinitionAbstract {
  /**
   *  @inheritDoc
   */
  static substituteModelDefinition() {
    return MnxOntologies.mnxAgent.ModelDefinitions.PersonDefinition;
  }

  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "halId",
        description: "HAL identifiant. @see https://hal.archives-ouvertes.fr/",
        rdfDataProperty: "ads:halId",
      }),
      new LiteralDefinition({
        literalName: "orcidId",
        description: "ORCID identifiant. @see https://orcid.org/",
        rdfDataProperty: "ads:orcidId",
      }),
    ];
  }
}
