/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  ModelDefinitionAbstract,
  MnxOntologies,
  GraphQLTypeDefinition,
  LiteralDefinition,
  LabelDefinition,
  LinkDefinition,
} from "@mnemotix/synaptix.js";
import PersonDefinition from "./PersonDefinition";

export default class FileDefinition extends ModelDefinitionAbstract {
  /**
   *  @inheritDoc
   */
  static substituteModelDefinition() {
    return MnxOntologies.mnxProject.ModelDefinitions.FileDefinition;
  }

  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: "hasLocation",
        rdfObjectProperty: "mnx:hasLocation",
        relatedModelDefinition:
          MnxOntologies.mnxGeo.ModelDefinitions.LocationDefinition,
        graphQLPropertyName: "location",
        graphQLInputName: "locationInput",
      }),
      new LinkDefinition({
        linkName: "hasReferralPerson",
        rdfObjectProperty: "ads:hasReferralPerson",
        relatedModelDefinition: PersonDefinition,
        isPlural: true,
        graphQLPropertyName: "referralPersons",
        graphQLInputName: "referralPersonInputs",
      }),
    ];
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "duration",
        rdfDataProperty: "dc:extent",
      }),
      new LiteralDefinition({
        literalName: "resourceType",
        rdfDataProperty: "dc:type",
      }),
      new LiteralDefinition({
        literalName: "temporalExtent",
        rdfDataProperty: "ads:temporalExtent",
      }),
      new LiteralDefinition({
        literalName: "spatialResolution",
        rdfDataProperty: "ads:spatialResolution",
      }),
    ];
  }

  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "title",
        rdfDataProperty: "dc:title",
      }),
      new LabelDefinition({
        labelName: "altTitle",
        rdfDataProperty: "dc:alternative",
      }),
      new LabelDefinition({
        labelName: "tableOfContents",
        rdfDataProperty: "dc:tableOfContents",
      }),
      new LabelDefinition({
        labelName: "notes",
        rdfDataProperty: "http://rs.tdwg.org/dwc/terms/eventRemarks",
      }),
      new LabelDefinition({
        labelName: "context",
        rdfDataProperty: "ads:context",
      }),
      new LabelDefinition({
        labelName: "rights",
        rdfDataProperty: "dc:rights",
      }),
    ];
  }
}
