/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  GraphQLTypeConnectionQuery,
  getObjectsResolver,
  getObjectsCountResolver,
} from "@mnemotix/synaptix.js";
import { openThesoClient } from "../../../../services/OpenThesoClient";
import ConceptDefinition from "../ConceptDefinition";

export class ConceptGraphQLTypeConnectionQuery extends GraphQLTypeConnectionQuery {
  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      [this.generateFieldName(modelDefinition)]: this.getConcepts.bind(this),
    });
  }

  /**
   * @param {*} _
   * @param {ResolverArgs} args
   * @param {SynaptixDatastoreRdfSession} synaptixSession
   * @param {GraphQLResolveInfo} gqlInfos
   */
  async getConcepts(_, args, synaptixSession, gqlInfos) {
    const localConcepts = await getObjectsResolver(
      ConceptDefinition,
      _,
      args,
      synaptixSession,
      gqlInfos
    );

    const { qs, limit, after, filters = [] } = args;
    const lang = synaptixSession.getContext().getLang() || "fr";

    // If filters, don't search in OpenTheso, juste in Koncept
    const remoteConcepts = !qs
      ? []
      : await openThesoClient.searchConcepts({ qs, lang });

    const concepts = this.processConceptsReconciliation(
      localConcepts.edges.map(({ node }) => node),
      remoteConcepts
    );

    return synaptixSession.wrapObjectsIntoGraphQLConnection(concepts, {
      limit,
      after,
    });
  }

  processConceptsReconciliation(localConcepts, remoteConcepts) {
    return [...localConcepts, ...remoteConcepts];
  }
}
