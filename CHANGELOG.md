### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

#### [2.1.1-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/2.0.0-beta.1...2.1.1-1)

- Bump depencies [`b42ab82`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/b42ab8281a9665bf8d9a9bb0870a1fd77fdf296c)
- [ic] Releasing 2.1.1-1 [`91f8bda`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/91f8bda5d21e61275d741b7df9a4e9ec9a2e709c)

#### [2.0.0-beta.1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.9.5-1...2.0.0-beta.1)

> 7 June 2022

- Upgraded to MUI V5 and add activity feeds [`#6`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/merge_requests/6)
- Improved feeds [`#5`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/merge_requests/5)
- Improved core EntityActivity widget to match with Arcadiis model [`#4`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/merge_requests/4)
- migrate to muiv5, remove some useStyle [`282d6a4`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/282d6a406b63c1ae5ecb8eda46fe6bbf16006992)
- trying to fix warning : Running multiple instances may cause problems [`dd3ba6d`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/dd3ba6d99e0685fc3a328a09e062e2934abcd6da)
- fix package.json [`cb6fa92`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/cb6fa92d7d55ab2130304e32eab00a117f416f7b)

#### [1.9.5-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.8.9-1...1.9.5-1)

> 29 March 2022

- Fixed #33 #32 [`#33`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/33)
- [ic] Releasing 1.9.5-1 [`4cb5037`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/4cb5037fa3471465718012c8bd30e0ff8b061817)

#### [1.8.9-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.8.7-1...1.8.9-1)

> 14 March 2022

- Updated dependencies [`74b537d`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/74b537dd91bd10d30ee4b1790a85eb0ddcd84449)
- Bump versions of Synaptix and Core [`89d15f9`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/89d15f9ca95d69bba7040b1f394ccd107c3bbf7c)
- [ic] Releasing 1.8.7-1 [`c022a00`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/c022a00b36375aa0e34357f5fdfe77b2b00d955f)

#### [1.8.7-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.8.6-1...1.8.7-1)

> 7 March 2022

- [ic] Releasing 1.8.7-1 [`7f499f9`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/7f499f99d77179467992f4e46413f426afe62e07)

#### [1.8.6-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.8.5-1...1.8.6-1)

> 4 March 2022

- Upgraded dependencies [`135124f`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/135124fb3d462386b048e4908cf0796e315dd703)
- [ic] Releasing 1.8.6-1 [`dc280a1`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/dc280a1e83357eb286777ebfb86cb05dfacc67c3)
- [ic] Releasing 1.8.6-1 [`a69f9b7`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/a69f9b7f2d078e925ed916c0ec9e56cd7d9c9017)

#### [1.8.5-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.8.1-1...1.8.5-1)

> 1 March 2022

- Updated dependencies [`8c463e4`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/8c463e459a2d99c9e747de06ea70542ed7ad54b3)
- [ic] Releasing 1.8.5-1 [`1c0eb0f`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/1c0eb0f56a88d48b4d69155669ffb8f6b7121dd3)

#### [1.8.1-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.22-1...1.8.1-1)

> 21 February 2022

- Partially implemented #20 [`9612d67`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/9612d67f853265c9c893bdef9b4d23a24f34aac1)
- [ic] Releasing 1.8.1-1 [`38fd491`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/38fd491f1dcc882b6f5e08bcf44603dde80fdb26)

#### [1.7.22-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.21-1...1.7.22-1)

> 5 February 2022

- Implemented #4 - Geographic resource preview (Work with shp.zip, wkt, gpx, geojson, kml, gtm, kmz file extensions. [`56e47c0`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/56e47c0f927547162b03b9cb781b1f15ce314a4e)
- Upgraded Weever dependencies [`37918c5`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/37918c58d724a7038ed5c1f76070c5626a933d3d)
- [ic] Releasing 1.7.22-1 [`e0277bf`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/e0277bf58384ff8e3fbb9231046220b0f0b9e992)

#### [1.7.21-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.18-1...1.7.21-1)

> 3 February 2022

- SplashScreen [`f3c58bb`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/f3c58bb04be44b93ee1691a5eb8a0aa1b35b169f)
- [ic] Releasing 1.7.21-1 [`e03e504`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/e03e5040de34418b7b6a4808b08a90f465443a44)

#### [1.7.18-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.17-1...1.7.18-1)

> 28 January 2022

- [ic] Releasing 1.7.18-1 [`b5ccf1f`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/b5ccf1f2f7766c22ba8274b9428cc9ebe4d71763)

#### [1.7.17-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.16-1...1.7.17-1)

> 24 January 2022

- Fixed #29 [`#29`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/29)
- [ic] Releasing 1.7.17-1 [`8a1439e`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/8a1439e1341b2d42b049f5c3c90c5e327734869a)

#### [1.7.16-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.15-1...1.7.16-1)

> 14 January 2022

- Implemented #8 [`600df7a`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/600df7a90a279efa3b74b4e266398babbbff9a2b)
- [ic] Releasing 1.7.16-1 [`88154c9`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/88154c9f620b991b398210a0a08d76c40ad89060)

#### [1.7.15-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.14-1...1.7.15-1)

> 13 January 2022

- Updated dependencies [`9183c52`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/9183c528cc08ccd8e58d44ddf6a8ae2df7f0e930)
- [ic] Releasing 1.7.15-1 [`6734a7c`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/6734a7c2de7db17d7397c6d63a882a1854a80a34)

#### [1.7.14-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.13-1...1.7.14-1)

> 6 January 2022

- Implemented #23 [`2bea58e`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/2bea58e166b130e26111e715d1f23afd0ef0e89c)
- [ic] Releasing 1.7.14-1 [`fc89fff`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/fc89fff003b6c93784b79305a31bc4124815e1ac)

#### [1.7.13-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.12-2...1.7.13-1)

> 4 January 2022

- Fixed #21 [`#21`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/21)
- Wip UX refactoring [`126320a`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/126320ab918bce0315de816e6b245503eb6a7355)
- Updated weever-core [`6809441`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/6809441aa3d2abdb584396a64bdbed6e6cb74224)
- [ic] Releasing 1.7.13-1 [`dfb6e57`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/dfb6e5721fefcf4a86f89fa67d7fbc076eee352f)

#### [1.7.12-2](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.12-1...1.7.12-2)

> 13 December 2021

- [ic] Releasing 1.7.12-2 [`98e6268`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/98e6268fae57311ff5410e57bf5a9f57f80af11b)
- Removed debugOnly flag on ResourceDynamicForm.js [`8543149`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/85431497578c7e3b800f711aa53f040ddaa3ee7e)

#### [1.7.12-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.10-1...1.7.12-1)

> 10 December 2021

- [ic] Releasing 1.7.12-1 [`6e8a41f`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/6e8a41fb5877f39afcbf2b1628f3e57836797b29)
- [ic] Releasing 1.7.12-1 [`61e175c`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/61e175c97f1d81ad5eecbe68b4ab93ff303a001f)

#### [1.7.10-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.9-1...1.7.10-1)

> 9 December 2021

- [ic] Releasing 1.7.10-1 [`3eb379f`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/3eb379f771f0e20fec7e71ae2431b96419709b7d)

#### [1.7.9-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.8-1...1.7.9-1)

> 9 December 2021

- Fixed #21 [`#21`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/21)
- [ic] Releasing 1.7.9-1 [`8caf03d`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/8caf03d5277dedf81395e52a5a16f14e43e68172)
- WIP [`ee3ab41`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/ee3ab4127bc5e2779327231f62f446831a931e60)
- Added validations on Person.form.js [`f6121b3`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/f6121b33eeab7ac04d36186a5c5b5044415f467e)

#### [1.7.8-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.7-2...1.7.8-1)

> 30 November 2021

- [ic] Releasing 1.7.8-1 [`c8ce5c3`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/c8ce5c3a452a152e6301affb934a18f5e8404b57)

#### [1.7.7-2](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.7-1...1.7.7-2)

> 30 November 2021

- Fixed bugs with mixed concepts locations (Opentheso and Koncept) [`81097b4`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/81097b4edc9f41d19df8c6a84cdcde2e7955bc3c)
- [ic] Releasing 1.7.7-2 [`968899a`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/968899a6a298b23e120bd22f1080ff92d319422a)

#### [1.7.7-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.6-1...1.7.7-1)

> 26 November 2021

- [ic] Releasing 1.7.7-1 [`cbf9ea6`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/cbf9ea63c14bd9b368918adb46fe7ba6b43645ba)

#### [1.7.6-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.5-1...1.7.6-1)

> 26 November 2021

- Fixed #22 [`#22`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/22)
- Fixed #21 [`#21`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/21)
- [ic] Releasing 1.7.6-1 [`882b288`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/882b288bf4dafe8495eb27a3d573ad2811ef03f7)

#### [1.7.5-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.7.4-1...1.7.5-1)

> 25 November 2021

- Updated dependencies [`e7b4209`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/e7b42098527da03627ef4fb1c55607ede83ea645)
- [ic] Releasing 1.7.5-1 [`ac29a18`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/ac29a18e08de7a6c7c808afc57a7a07e886e98b1)

#### [1.7.4-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.3.1...1.7.4-1)

> 22 November 2021

- Update dependencies [`#3`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/merge_requests/3)
- Merge branch 'update-core' into 'master' [`#15`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/15)
- Updated dependencies. [`7d7e3df`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/7d7e3dfcef2be812312d31848ec9b81462a4610b)
- Add Humanum config and README. [`c8f5395`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/c8f539526d97f29f2a8e2cdba756708026e1ab79)
- Added missing dependencies [`3fe2c82`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/3fe2c823b1d598dec2d41bb9e9eb2f16238e8cfd)

#### [1.3.1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.3.0...1.3.1)

> 29 June 2021

- Fixed #15 [`#15`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/15)
- [ic] Releasing 1.3.1 [`8dfe5f2`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/8dfe5f25c01ab3a81a8529bee49f083c55de82da)

#### [1.3.0](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.1.0...1.3.0)

> 25 June 2021

- Updated dependencies [`eaece42`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/eaece42a90b1c396149035f618a0a3a37ec54df7)
- [ic] Releasing 1.3.0 [`817ea0d`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/817ea0d73b6cc68cb843e9693e4ce6d979533231)

#### [1.1.0](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.1.0-beta.3.1...1.1.0)

> 17 June 2021

- update core & yarn berry & ResourceDynamicForm [`696e8ce`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/696e8cee6a4ebb45b0e4c477c40ab26d934b3978)
- [ic] Releasing 1.1.0 [`f69666e`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/f69666e3cd6bacbc87f16e69d84197f3916764e6)

#### [1.1.0-beta.3.1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.22.20201020-1...1.1.0-beta.3.1)

> 18 December 2020

- Updated dependencies (webpack 5, apollo-client V3) [`3f56fc8`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/3f56fc8d9c693e2b45d47fd424a4cf66f7fb865d)

#### [1.0.0-beta.22.20201020-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.22.20201019-1...1.0.0-beta.22.20201020-1)

> 20 October 2020

- Upgraded Synaptix.js and fixed #12 [`#12`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/12)
- Update AppBarTitleComponent.js. Suppression "Editer les données de l'archéologie" [`a2b1fb0`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/a2b1fb0149a4381364d3285a192f1752f1fe4217)

#### [1.0.0-beta.22.20201019-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.20.20201016-3...1.0.0-beta.22.20201019-1)

> 19 October 2020

- Upgrade weeve-core to 1.0.0-beta.22 [`7a399ad`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/7a399ad94a222e3b5a9aa4aa4d290cfb953297a7)

#### [1.0.0-beta.20.20201016-3](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.20.20201016-2...1.0.0-beta.20.20201016-3)

> 16 October 2020

- Upgraded Synaptix.js and fixed #9 [`#9`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/9)
- Implemented #7 [`c21fae8`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/c21fae893e559f8f08fd55c6a3293fb9b8d670c6)

#### [1.0.0-beta.20.20201016-2](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.20.20201016-1...1.0.0-beta.20.20201016-2)

> 16 October 2020

- Fixed CSS margin. [`6a754e6`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/6a754e6a93537c8cdb2c0bd351344195c8c72b49)

#### [1.0.0-beta.20.20201016-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.18.20201001-1...1.0.0-beta.20.20201016-1)

> 16 October 2020

- Update README.md [`#2`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/merge_requests/2)
- WIP #6 [`40d7923`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/40d79239d5c5ef717cb754191913973867343e1a)
- Upgraded Synaptix.js [`1c690d5`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/1c690d5235ee420c38c14a9e304b9703430a5065)
- Upgrade synaptix.js to 4.6.0-rc.9 [`c30c6c6`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/c30c6c6f043f9802f219546c328e3ac5df7c402b)

#### [1.0.0-beta.18.20201001-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.16.20200922-1...1.0.0-beta.18.20201001-1)

> 1 October 2020

- Remplacement favicon par fichier png avec transparence [`#1`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/merge_requests/1)
- Fixed #2 [`#2`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/2)
- Fixed #1 [`#1`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/issues/1)
- Upgrade weeve-core to 1.0.0-beta.17 [`e1e77db`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/e1e77db18a43510684fbd793b6ab93b16168330f)
- Upgrade weeve-core to 1.0.0-beta.18 [`c67186f`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/c67186f74795d0a7d8c4b841086e4ae8e2c461ec)

#### [1.0.0-beta.16.20200922-1](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.15.20200921-2...1.0.0-beta.16.20200922-1)

> 22 September 2020

#### [1.0.0-beta.15.20200921-2](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/compare/1.0.0-beta.15.20200921-1...1.0.0-beta.15.20200921-2)

> 21 September 2020

- Upgraded @mnemotix/synaptix.js to 4.6.0-rc.6 [`5b3d41f`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/5b3d41f106d9e2f5ee691c1718c0f313f4bea847)

#### 1.0.0-beta.15.20200921-1

> 21 September 2020

- First commit [`ecbc39e`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/ecbc39e2dd33e035ad1f8956cb38a1a8120ff747)
- Upgraded weeve-core to 1.0.0-beta.15 [`7ce67fc`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/7ce67fc0649b42ca1ffc1054654b7167144592c0)
- Temporarly enable full feature [`6bc5ed2`](https://gitlab.com/mnemotix/weever-community/weever-arcadiis/commit/6bc5ed200c2286d803305d988f9633ef64da6199)
